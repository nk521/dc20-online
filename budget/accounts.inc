account assets:cash
account assets:debian-france
account assets:SPI

account expenses:bursaries:expense
account expenses:fees
account expenses:general
account expenses:incidentals
account expenses:swag:t-shirt
account expenses:swag:shipping
account expenses:tax
account expenses:video
account income:registration
account income:registration:stripe for debian-france
account income:registration:SEPA for debian-france
account income:sponsors:bronze
account income:sponsors:gold
account income:sponsors:platinum
account income:sponsors:silver
account income:sponsors:supporter
account income:sponsors:donations

account liabilities:nattie
